/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "rtc.h"
#include "usart.h"
#include "usb.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

	#include <stdio.h>
	#include <string.h>
	#include <stdlib.h>
	#include "Local_config.h"
	#include "one-pin-debug-sm.h"
	#include "tm1637_sm.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

	char 		DataChar[0xFF]	= { 0 } ;
	uint32_t	adc_u32[3]		= { 0 } ;
	uint32_t	volt_u32		= 0		;
	int			amper_i			= 0		;
	uint8_t		akb_status		= 1		;
	uint8_t		current_status	= 0		;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

	void UartDebug(char* _text);
	void PrintSoftVersion(uint32_t _soft_version_u32);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_RTC_Init();
  MX_USART2_UART_Init();
  MX_USB_PCD_Init();
  MX_ADC1_Init();
  /* USER CODE BEGIN 2 */

	hrtc.Init.OutPut=RTC_OUTPUTSOURCE_NONE;
	HAL_RTC_Init(&hrtc);

	sprintf(DataChar,"\r\n\r\n\t Home-2024 Control AKB 12V\r\n" ); UartDebug(DataChar) ;
	PrintSoftVersion(SOFT_VERSION);
	HAL_ADC_Start_DMA(&hadc1, adc_u32, 3);

	tm1637_struct h1_tm1637;
	h1_tm1637.clk_pin  = GPIO_PIN_6;
	h1_tm1637.clk_port = GPIOB;
	h1_tm1637.dio_pin  = GPIO_PIN_7;
	h1_tm1637.dio_port = GPIOB;
	h1_tm1637.digit_qnt= 6;
	// h1_tm1637.digit_qnt= 4;
	TM1637_Init(&h1_tm1637);

	TM1637_Set_Brightness(&h1_tm1637, bright_full);
	TM1637_Display_Decimal(&h1_tm1637, 123456 , double_dot);
	//TM1637_Display_Decimal(&h1_tm1637, 1234 , double_dot);
	HAL_Delay(1000);
	TM1637_Set_Brightness(&h1_tm1637, bright_15percent);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, RESET);
	HAL_Delay(50);
	HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, SET);

	amper_i = 5*adc_u32[1] - 13650;
	sprintf(DataChar,"Current:%lu, I:%04dmA \t", adc_u32[1], amper_i); UartDebug(DataChar);

	if (amper_i < -100) {
		current_status = 0;
	} else {
		current_status = 1;
	}
	volt_u32 = adc_u32[0]*507/10000 + 4;
	sprintf(DataChar,"v1=%lu; v2=%lu\r\n ", adc_u32[0]/19, volt_u32 ); UartDebug(DataChar) ;

	if (amper_i < 0) {
		amper_i = amper_i* (-1);
	}

	TM1637_Display_Decimal(&h1_tm1637, volt_u32*1000 + amper_i/10, no_double_dot);
	// TM1637_Display_Decimal(&h1_tm1637, volt_u32*10, no_double_dot);
	if ( volt_u32 > 125 ) {
		akb_status = 0;
		HAL_GPIO_WritePin(RELAY_GPIO_Port, RELAY_Pin, SET);
	}
	if ( volt_u32 < 120 ) {
		akb_status = 1;
		HAL_GPIO_WritePin(RELAY_GPIO_Port, RELAY_Pin, RESET);
	}
	HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, akb_status);
	HAL_Delay(1000);

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_ADC
                              |RCC_PERIPHCLK_USB;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

void UartDebug(char* _text) {
#ifdef MY_DEBUG
	HAL_UART_Transmit(UART_DEBUG, (uint8_t*)_text, strlen(_text), 100);
#endif
#ifdef MY_DEBUG_ONE_PIN
	One_Pin_Debug_Print((uint8_t*)_text, strlen(_text));
#endif
} //**************************************************************************

void PrintSoftVersion(uint32_t _soft_version_u32) {
	int soft_version_arr_int[3];
	soft_version_arr_int[0] = (_soft_version_u32 / 100)     ;
	soft_version_arr_int[1] = (_soft_version_u32 /  10) %10 ;
	soft_version_arr_int[2] = (_soft_version_u32      ) %10 ;

	sprintf(DataChar," v%d.%d.%d \r\n",
						soft_version_arr_int[0] ,
						soft_version_arr_int[1] ,
						soft_version_arr_int[2] ); UartDebug(DataChar);

	#define 	DATE_as_int_str 	(__DATE__)
	#define 	TIME_as_int_str 	(__TIME__)
	sprintf(DataChar,"\tBuild: %s. Time: %s.\r\n" ,
						DATE_as_int_str ,
						TIME_as_int_str ); UartDebug(DataChar);
}//**************************************************************************
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
